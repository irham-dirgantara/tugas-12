@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="first-name"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="last-name"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
            <option value="Thailand">Thailand</option>
            <option value="Vietnam">Vietnam</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="2">English <br>
        <input type="checkbox" name="language" value="3">Other <br><br>
        <label>Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
    @endsection