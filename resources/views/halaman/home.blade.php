<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 - Welcome</title>
</head>
<body>
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}} !</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
    <p>Jenis Kelamin = 
        @if($jenisKelamin === "1")
            Laki - Laki
        @elseif($jenisKelamin === "2")
            Perempuan
        @else
            Lainnya
        @endif
    </p>
</body>
</html>