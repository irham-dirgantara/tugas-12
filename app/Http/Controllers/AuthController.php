<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('halaman.form');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['first-name'];
        $namaBelakang = $request['last-name'];
        $jenisKelamin = $request['gender'];
        $kebangsaan = $request['nationality'];
        $bahasa = $request['language'];
        $bio = $request['bio'];

        return view('halaman.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang,
        'jenisKelamin' => $jenisKelamin, 'kebangsaan' => $kebangsaan, 'bahasa' => $bahasa,
        'bio' => $bio]);
    }
}
